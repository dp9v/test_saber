from django.conf.urls import url
from content.views import IndexView

urlpatterns = [
	url(r'^$', IndexView.as_view(), name='home'),
]
