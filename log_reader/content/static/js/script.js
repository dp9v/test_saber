
$(document).ready(function () {
    $('#update_log').submit(update_log);
});

function update_log(event) {
    event.preventDefault();
    form = $(this);
    url = event.currentTarget.action;
    data = form.serialize();
    $.post(url, data, success, 'json').fail(error);

    return false;

    function success(data) {
        if (data.ok){
            $("#offset").val(data.next_offset);
        }
        $("#json_area").val(JSON.stringify(data));
    }

    function error() {
        alert("Ошибка");
    }
}