# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.conf import settings
from django.views.generic.base import TemplateView
import jsonlines
import json



class IndexView(TemplateView):
    template_name = "index.html"

    def post(self, request):
        offset = request.POST.get("offset")
        try:
            res = read_lines(offset)
            res["ok"] = True
        except Exception as ex:
            res = {'ok': False, 'reason': str(ex)}

        return HttpResponse(json.dumps(res), content_type="application/json")


def read_lines(offset):
    """
    Считывает из лога количество строк = CHUNK_SIZE
    :param offset: строка с которой надо начинать считывание
    :return: список строк с дополнительными параметрам
    """
    if offset is None:
        raise Exception("No POST parameter: offset")
    else:
        offset = int(offset)
    current = 0
    messages = []
    with jsonlines.open(settings.LOG_PATH, 'r') as reader:
        for line in reader:
            if offset <= current < offset + settings.CHUNK_SIZE:
                messages.append(line)
            current += 1

    next_offset = offset + settings.CHUNK_SIZE if offset + settings.CHUNK_SIZE < current else current
    if len(messages) == 0:
        raise Exception("file is finished")

    return {
        "next_offset": next_offset,
        "total_size": current,
        "messages": messages
    }
